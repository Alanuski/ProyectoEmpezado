package controladores;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SingletonID {

    private static SingletonID instance;
    private static String nombre;
    private int ID;

    public static void setNombre(String nombre) {
        SingletonID.nombre = nombre;
    }
    

    private SingletonID() {

    }
    
    public int getID() throws SQLException{
        /*if(instance == null){
            instance = new SingletonID();
        }*/
    
        MySQLBD mysql = new MySQLBD().conectar();
        ResultSet resultadoID = mysql.consultar("SELECT usuario_id from usuarios WHERE nombreUsuario = " + "'"
                + nombre + "'" + ";");

        if (resultadoID != null) {
            try {
                resultadoID.next();
                System.out.println(resultadoID.getInt("usuario_id"));
            } catch (SQLException ex) {
                Logger.getLogger(ControladorIniciarSesion.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        //return resultadoID.getInt("usuario_id");
    //}
        this.ID = resultadoID.getInt("usuario_id");
        return resultadoID.getInt("usuario_id");
    }
}
