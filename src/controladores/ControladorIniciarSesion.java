package controladores;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import static java.lang.String.valueOf;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import vistas.JFFame;
import java.lang.Integer;
import vistas.JFrameAgregar;
import vistas.JFrameIniciarSesion;
import static java.lang.String.valueOf;

public class ControladorIniciarSesion implements ActionListener {

    public String nombre;
    public int ID;

    JFrameIniciarSesion vistaIniciarSesion = new JFrameIniciarSesion();

    public ControladorIniciarSesion(JFrameIniciarSesion vistaIniciarSesion) {
        this.vistaIniciarSesion = vistaIniciarSesion;
        this.vistaIniciarSesion.jButtonAccederLog.addActionListener((ActionListener) this);

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String nombre = vistaIniciarSesion.jTextFieldUsuarioLog.getText();
        this.nombre = nombre;
        char[] contrasenya = vistaIniciarSesion.jPasswordFieldContrasenyaLog.getPassword();
        MySQLBD mysql = new MySQLBD().conectar();
        ResultSet resultadoNombre = mysql.consultar("SELECT nombreUsuario from usuarios WHERE nombreUsuario = " + "'"
                + nombre + "'" + ";");
        if (resultadoNombre != null) {
            try {
                if (resultadoNombre.next()) {
                    System.out.println(resultadoNombre.getString("nombreUsuario"));
                    if (resultadoNombre.getString("NombreUsuario").equals(nombre.toString())) {
                        System.out.println("El nombre es correcto");
                        System.out.println(String.valueOf(contrasenya));
                        ResultSet consultaContrasenya = mysql.consultar("SELECT password from usuarios WHERE nombreUsuario = " + "'"
                                + nombre + "'" + ";");
                        if (consultaContrasenya.next()) {
                            System.out.println(consultaContrasenya.getString("password"));
                            if (consultaContrasenya.getString("password").equals(String.valueOf(contrasenya))) {
                                JOptionPane.showMessageDialog(vistaIniciarSesion, "Usuario y contraseña correctos");
                                getID();
                                JFFame fame = new JFFame();
                                fame.setVisible(true);
                            } else {
                                System.out.println("La password es incorrecto");
                                JOptionPane.showMessageDialog(vistaIniciarSesion, "La contraseña es incorrecta");
                                vistaIniciarSesion.dispose();
                                JFrameIniciarSesion vistaIniciarSesion = new JFrameIniciarSesion();
                                vistaIniciarSesion.setVisible(true);
                            }

                        } else {
                            System.out.println("La contraseña es incorrecta");
                            JOptionPane.showMessageDialog(vistaIniciarSesion, "La contraseña es incorrecta");
                            vistaIniciarSesion.dispose();
                            JFrameIniciarSesion vistaIniciarSesion = new JFrameIniciarSesion();
                            vistaIniciarSesion.setVisible(true);
                        }

                    } else {
                        System.out.println("El nombre es incorrecto");
                        JOptionPane.showMessageDialog(vistaIniciarSesion, "El usuario es incorrecto");
                        vistaIniciarSesion.dispose();
                        JFrameIniciarSesion vistaIniciarSesion = new JFrameIniciarSesion();
                        vistaIniciarSesion.setVisible(true);
                    }
                } else {
                    System.out.println("El nombre es incorrecto");
                    JOptionPane.showMessageDialog(vistaIniciarSesion, "El usuario es incorrecto");
                    vistaIniciarSesion.dispose();
                    JFrameIniciarSesion vistaIniciarSesion = new JFrameIniciarSesion();
                    vistaIniciarSesion.setVisible(true);
                }

            } catch (SQLException ex) {
                Logger.getLogger(ControladorIniciarSesion.class.getName()).log(Level.SEVERE, null, ex);

            }

        } else {
            System.out.println("El nombre es incorrecto");
            JOptionPane.showMessageDialog(vistaIniciarSesion, "El usuario es incorrecto");
            vistaIniciarSesion.dispose();
            JFrameIniciarSesion vistaIniciarSesion = new JFrameIniciarSesion();
            vistaIniciarSesion.setVisible(true);
        }

    }

    public int getID() throws SQLException {
        MySQLBD mysql = new MySQLBD().conectar();
        ResultSet resultadoID = mysql.consultar("SELECT usuario_id from usuarios WHERE nombreUsuario = " + "'"
                + this.nombre + "'" + ";");

        if (resultadoID != null) {
            try {
                resultadoID.next();
                System.out.println(resultadoID.getInt("usuario_id"));
            } catch (SQLException ex) {
                Logger.getLogger(ControladorIniciarSesion.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        this.ID  =resultadoID.getInt("usuario_id");
        return resultadoID.getInt("usuario_id");
    }

}
