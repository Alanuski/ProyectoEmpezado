package controladores;

import java.sql.ResultSet;

public class PruebaSelect {

    public static void main(String[] args) {
        MySQLBD baseDatos = new MySQLBD().conectar();
        
        ResultSet resultados = baseDatos.consultar("SELECT * FROM cuentas WHERE cuenta_id=12");
        if (resultados != null) {
            try {
                System.out.println("NumeroIban       TipoCuenta          Saldo");
                System.out.println("--------------------------------");
                //resultados.next();
                //System.out.println(resultados.getString("numeroIban"));
                while (resultados.next()) {
                    System.out.println("" + resultados.getString("numeroIban") + "       " + resultados.getString("fechaCreacion")
                            +"  "+ resultados.getDouble("saldo"));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

}
