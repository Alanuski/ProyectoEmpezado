/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladores;

import com.alexco.db.exceptions.DBDriverNotFound;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import modelos.UsuarioDAO;
import vistas.JFrameAgregar;
import vistas.JFrameUsuarioNuevo;

/**
 *
 * @author root
 */
public class ControladorUsuarioNuevo implements ActionListener {

    JFrameUsuarioNuevo vistaUsuarioNuevo = new JFrameUsuarioNuevo();
    UsuarioDAO modeloUsuario = new UsuarioDAO();

    public ControladorUsuarioNuevo() {

    }

    public ControladorUsuarioNuevo(JFrameUsuarioNuevo controladorUsuarioNuevo, UsuarioDAO modeloUsuario) {
        this.vistaUsuarioNuevo = controladorUsuarioNuevo;
        this.modeloUsuario = modeloUsuario;
        this.vistaUsuarioNuevo.jButtonContinuar.addActionListener(this);

    }
    public void InicializarUsuario(){
       JFrameUsuarioNuevo ventanaUsuarioNuevo=new JFrameUsuarioNuevo();
       ventanaUsuarioNuevo.setVisible(true);
       
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        char[] contrasenya = vistaUsuarioNuevo.jPasswordFieldContrasenya.getPassword();
        char[] repitaContrasenya = vistaUsuarioNuevo.jPasswordFieldRepitaContrasenya.getPassword();
        System.out.println("funciona");
        System.out.println("1: " + Arrays.toString(contrasenya));
        System.out.println("2: " +Arrays.toString(repitaContrasenya));
        if (!Arrays.toString(contrasenya).equals(Arrays.toString(repitaContrasenya))) {
            JOptionPane.showMessageDialog(vistaUsuarioNuevo, "Las contraseñas no coinciden");

        } else {
            String nombre = vistaUsuarioNuevo.jTextFieldNombe.getText();
            String apellido = vistaUsuarioNuevo.jTextFieldApellido.getText();
            String DNI = vistaUsuarioNuevo.jTextFieldDNI.getText();
            String email = vistaUsuarioNuevo.jTextFieldEmail.getText();
            modeloUsuario.setNombreUsuario(nombre);
            modeloUsuario.setApellido(apellido);
            modeloUsuario.setDNI(DNI);
            modeloUsuario.setEmail(email);
            modeloUsuario.setPassword(String.valueOf(contrasenya));
            //modeloUsuario.setPassword(Arrays.toString(contrasenya));
            try {
                modeloUsuario.save();
                JOptionPane.showMessageDialog(vistaUsuarioNuevo, "Se ha creado su usuario exitosamente");
                JOptionPane.showMessageDialog(vistaUsuarioNuevo, "Bienvenido");
                
            } catch (DBDriverNotFound ex) {
                Logger.getLogger(ControladorUsuarioNuevo.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SQLException ex) {
                Logger.getLogger(ControladorUsuarioNuevo.class.getName()).log(Level.SEVERE, null, ex);
            }
            vistaUsuarioNuevo.dispose();
            JFrameAgregar Agregar = new JFrameAgregar();
            Agregar.setVisible(true);
        }
    }
}
