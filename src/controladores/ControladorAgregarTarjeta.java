
package controladores;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import modelos.TarjetaDAO;
import vistas.JFrameAgregar;
import vistas.JFrameAgregarTarjeta;


public class ControladorAgregarTarjeta implements ActionListener {
    JFrameAgregar vistaAgregar = new JFrameAgregar();
    TarjetaDAO modeloTarjeta = new TarjetaDAO();
    
    public ControladorAgregarTarjeta(){
        
    }
    public ControladorAgregarTarjeta(JFrameAgregar controladorAgregarTarjeta, TarjetaDAO modeloTarjeta){
        this.vistaAgregar = controladorAgregarTarjeta;
        this.modeloTarjeta = modeloTarjeta;
        this.vistaAgregar.jButtonAgregarTarjeta.addActionListener(this);
        
    }
    @Override
    public void actionPerformed(ActionEvent e){
     JFrameAgregarTarjeta AgregarTarjeta = new JFrameAgregarTarjeta();
     AgregarTarjeta.setVisible(true);
     
    }
    
    
}
