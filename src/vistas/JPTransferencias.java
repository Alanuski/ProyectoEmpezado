/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vistas;

/**
 *
 * @author root
 */
public class JPTransferencias extends javax.swing.JPanel {

    /**
     * Creates new form JPTransferencias
     */
    public JPTransferencias() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel2 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jComboBox1 = new javax.swing.JComboBox<>();
        jComboBox2 = new javax.swing.JComboBox<>();
        jComboBox3 = new javax.swing.JComboBox<>();
        jComboBox4 = new javax.swing.JComboBox<>();
        jComboBox5 = new javax.swing.JComboBox<>();
        jComboBox6 = new javax.swing.JComboBox<>();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jButton1 = new javax.swing.JButton();

        setMinimumSize(new java.awt.Dimension(649, 400));
        setPreferredSize(new java.awt.Dimension(649, 400));
        setLayout(null);

        jLabel2.setFont(new java.awt.Font("Calibri", 1, 24)); // NOI18N
        jLabel2.setText("Aqui puedes consultar tus transferencias");
        add(jLabel2);
        jLabel2.setBounds(30, 10, 410, 30);

        jLabel1.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        jLabel1.setText("Selecciona las fechas que quieras consultar");
        add(jLabel1);
        jLabel1.setBounds(30, 50, 260, 40);

        jLabel3.setText("Desde");
        add(jLabel3);
        jLabel3.setBounds(70, 90, 50, 30);

        jLabel4.setText("Hasta");
        add(jLabel4);
        jLabel4.setBounds(70, 130, 50, 30);

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Día" }));
        add(jComboBox1);
        jComboBox1.setBounds(130, 90, 46, 25);

        jComboBox2.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Día" }));
        add(jComboBox2);
        jComboBox2.setBounds(130, 130, 46, 25);

        jComboBox3.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Mes" }));
        add(jComboBox3);
        jComboBox3.setBounds(210, 90, 52, 25);

        jComboBox4.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Mes" }));
        add(jComboBox4);
        jComboBox4.setBounds(210, 130, 52, 25);

        jComboBox5.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Año" }));
        add(jComboBox5);
        jComboBox5.setBounds(290, 90, 50, 25);

        jComboBox6.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Año" }));
        add(jComboBox6);
        jComboBox6.setBounds(290, 130, 50, 25);

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null}
            },
            new String [] {
                "Fecha envio", "Destinatario", "Moneda local", "Moneda extranjerab", "Cuenta Bancaria"
            }
        ));
        jScrollPane1.setViewportView(jTable1);

        add(jScrollPane1);
        jScrollPane1.setBounds(0, 180, 650, 220);

        jButton1.setText("Agregar transferencia");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        add(jButton1);
        jButton1.setBounds(470, 10, 170, 40);
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
JFrameAgregarTransferencia trans= new JFrameAgregarTransferencia();
trans.setVisible(true);
this.dispose();

        // TODO add your handling code here:
    }//GEN-LAST:event_jButton1ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton jButton1;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JComboBox<String> jComboBox2;
    private javax.swing.JComboBox<String> jComboBox3;
    private javax.swing.JComboBox<String> jComboBox4;
    private javax.swing.JComboBox<String> jComboBox5;
    private javax.swing.JComboBox<String> jComboBox6;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    // End of variables declaration//GEN-END:variables

    private void dispose() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
