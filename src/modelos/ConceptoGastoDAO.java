package modelos;

import com.alexco.db.dao.DAOBase;
import com.alexco.db.dao.DAOField;

public class ConceptoGastoDAO extends DAOBase {

    private String Descripcion;
    private double Valor;
    private String Emisor;
    private int conceptoGasto_id;

    public int getConceptoGasto_id() {
        return conceptoGasto_id;
    }

    public void setConceptoGasto_id(int conceptoGasto_id) {
        this.conceptoGasto_id = conceptoGasto_id;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public String getEmisor() {
        return Emisor;
    }

    public double getValor() {
        return Valor;
    }

    public void setDescripcion(String Descripcion) {
        this.Descripcion = Descripcion;
    }

    public void setEmisor(String Emisor) {
        this.Emisor = Emisor;
    }

    public void setValor(double Valor) {
        this.Valor = Valor;
    }

    public ConceptoGastoDAO() {
        super("ConceptosGastos");
        addFieldDefinition(new DAOField("ConceptoGasto_id", java.sql.Types.INTEGER, true));
        addFieldDefinition(new DAOField("Descripcion", java.sql.Types.VARCHAR, 100));
        addFieldDefinition(new DAOField("Emisor", java.sql.Types.VARCHAR, 100));
        addFieldDefinition(new DAOField("Valor", java.sql.Types.DOUBLE));
    }

}
