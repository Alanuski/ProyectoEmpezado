package modelos;

import com.alexco.db.dao.DAOBase;
import com.alexco.db.dao.DAOField;

public class TemaDAO extends DAOBase {

    private String tituloTema;
    private int tema_id;

    public int getTema_id() {
        return tema_id;
    }

    public void setTema_id(int tema_id) {
        this.tema_id = tema_id;
    }

    public String getTituloTema() {
        return tituloTema;
    }

    public void setTituloTema(String tituloTema) {
        this.tituloTema = tituloTema;
    }

    public TemaDAO() {
        super("Temas");
        addFieldDefinition(new DAOField("TituloTema", java.sql.Types.VARCHAR, 60));
        addFieldDefinition(new DAOField("Tema_id", java.sql.Types.INTEGER, true));
    }
}
