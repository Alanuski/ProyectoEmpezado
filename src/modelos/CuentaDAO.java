package modelos;

import com.alexco.db.dao.DAOBase;
import com.alexco.db.dao.DAOField;
import java.sql.Date;

public class CuentaDAO extends DAOBase {

    private int cuenta_id;
    private Date fechaCreacion;
    private Integer tipoCuenta;
    private Double saldo;
    private String numeroIban;
    private String descripcion;

    public int getCuenta_id() {
        return cuenta_id;
    }

    public void setCuenta_id(int cuenta_id) {
        this.cuenta_id = cuenta_id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public String getNumeroIban() {
        return numeroIban;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public Integer getTipoCuenta() {
        return tipoCuenta;
    }

    public Double getSaldo() {
        return saldo;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void setNumeroIban(String numeroIban) {
        this.numeroIban = numeroIban;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public void setTipoCuenta(Integer tipoCuenta) {
        this.tipoCuenta = tipoCuenta;
    }

    public void setSaldo(Double saldo) {
        this.saldo = saldo;
    }

    public CuentaDAO() {
        super("Cuentas");
        addFieldDefinition(new DAOField("Cuenta_id", java.sql.Types.INTEGER, true));
        addFieldDefinition(new DAOField("Descripcion", java.sql.Types.VARCHAR, 200));
        addFieldDefinition(new DAOField("NumeroIBAN", java.sql.Types.VARCHAR, 200));
        addFieldDefinition(new DAOField("FechaCreacion", java.sql.Types.DATE));
        addFieldDefinition(new DAOField("TipoCuenta_id", java.sql.Types.INTEGER, 11));
        addFieldDefinition(new DAOField("Saldo", java.sql.Types.DOUBLE));

    }

}
