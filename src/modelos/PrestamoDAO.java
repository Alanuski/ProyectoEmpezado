package modelos;

import com.alexco.db.dao.DAOBase;
import com.alexco.db.dao.DAOField;
import java.sql.Date;

public class PrestamoDAO extends DAOBase {

    private String descripcion;
    private int prestamo_id;

    public int getPrestamo_id() {
        return prestamo_id;
    }

    public void setPrestamo_id(int prestamo_id) {
        this.prestamo_id = prestamo_id;
    }
    private int cuenta_id;
    private int tipoPeriodo_id;
    private Date fechaCreacion;
    private Date fechaVencimiento;
    private int numCuotas;
    private double tipoTAE;
    private double limite;
    private double saldo;

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getCuenta_id() {
        return cuenta_id;
    }

    public void setCuenta_id(int cuenta_id) {
        this.cuenta_id = cuenta_id;
    }

    public int getTipoPeriodo_id() {
        return tipoPeriodo_id;
    }

    public void setTipoPeriodo_id(int tipoPeriodo_id) {
        this.tipoPeriodo_id = tipoPeriodo_id;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Date getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setFechaVencimiento(Date fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public int getNumCuotas() {
        return numCuotas;
    }

    public void setNumCuotas(int numCuotas) {
        this.numCuotas = numCuotas;
    }

    public double getTipoTAE() {
        return tipoTAE;
    }

    public void setTipoTAE(double tipoTAE) {
        this.tipoTAE = tipoTAE;
    }

    public double getLimite() {
        return limite;
    }

    public void setLimite(double limite) {
        this.limite = limite;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public PrestamoDAO() {
        super("Prestamos");
        addFieldDefinition(new DAOField("Prestamo_id", java.sql.Types.INTEGER, true));
        addFieldDefinition(new DAOField("Descripcion", java.sql.Types.VARCHAR, 200));
        addFieldDefinition(new DAOField("Cuenta_id", java.sql.Types.INTEGER, false));
        addFieldDefinition(new DAOField("TipoPeriodo_id", java.sql.Types.INTEGER, false));
        addFieldDefinition(new DAOField("FechaCreacion", java.sql.Types.DATE));
        addFieldDefinition(new DAOField("FechaVencimiento", java.sql.Types.DATE));
        addFieldDefinition(new DAOField("NumCuotas", java.sql.Types.INTEGER, false));
        addFieldDefinition(new DAOField("TipoTAE", java.sql.Types.DOUBLE));
        addFieldDefinition(new DAOField("Limite", java.sql.Types.DOUBLE));
        addFieldDefinition(new DAOField("Saldo", java.sql.Types.DOUBLE));
    }

}
