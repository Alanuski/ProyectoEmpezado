package modelos;

import com.alexco.db.dao.DAOField;
import java.sql.Date;
import com.alexco.db.dao.DAOBase;

public class ConceptoIngresoDAO extends DAOBase {

    private String descripcion;
    private int conceptosIngresos_id;

    public int getConceptosIngresos_id() {
        return conceptosIngresos_id;
    }

    public void setConceptosIngresos_id(int conceptosIngresos_id) {
        this.conceptosIngresos_id = conceptosIngresos_id;
    }
    private String emisor;
    private int cuenta_id;
    private int tipoperiodicidad_id;
    private Date fechaCreacion;
    private Date fechaBaja;
    private double valor;
    private boolean esExacto;

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getEmisor() {
        return emisor;
    }

    public void setEmisor(String emisor) {
        this.emisor = emisor;
    }

    public int getCuenta_id() {
        return cuenta_id;
    }

    public void setCuenta_id(int cuenta_id) {
        this.cuenta_id = cuenta_id;
    }

    public int getTipoperiodicidad_id() {
        return tipoperiodicidad_id;
    }

    public void setTipoperiodicidad_id(int tipoperiodicidad_id) {
        this.tipoperiodicidad_id = tipoperiodicidad_id;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Date getFechaBaja() {
        return fechaBaja;
    }

    public void setFechaBaja(Date fechaBaja) {
        this.fechaBaja = fechaBaja;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public boolean isEsExacto() {
        return esExacto;
    }

    public void setEsExacto(boolean esExacto) {
        this.esExacto = esExacto;
    }

    public ConceptoIngresoDAO() {
        super("ConceptosIngresos");
        addFieldDefinition(new DAOField("ConceptoIngreso_id", java.sql.Types.INTEGER, true));
        addFieldDefinition(new DAOField("Descripcion", java.sql.Types.VARCHAR, 200));
        addFieldDefinition(new DAOField("Emisor", java.sql.Types.VARCHAR, 200));
        addFieldDefinition(new DAOField("Cuenta_id", java.sql.Types.INTEGER, false));
        addFieldDefinition(new DAOField("TipoPeriodo_id", java.sql.Types.INTEGER, false));
        addFieldDefinition(new DAOField("FechaCreacion", java.sql.Types.DATE));
        addFieldDefinition(new DAOField("Valor", java.sql.Types.DOUBLE));
        addFieldDefinition(new DAOField("EsExacto", java.sql.Types.BOOLEAN));
    }

}
