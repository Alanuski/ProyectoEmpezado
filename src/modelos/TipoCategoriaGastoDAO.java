package modelos;

import com.alexco.db.dao.DAOBase;
import com.alexco.db.dao.DAOField;

public class TipoCategoriaGastoDAO extends DAOBase {

    private String descripcion;
    private int tipoCategoriaGasto_id;

    public int getTipoCategoriaGasto_id() {
        return tipoCategoriaGasto_id;
    }

    public void setTipoCategoriaGasto_id(int tipoCategoriaGasto_id) {
        this.tipoCategoriaGasto_id = tipoCategoriaGasto_id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public TipoCategoriaGastoDAO() {
        super("tiposcategoriasgastos");
        addFieldDefinition(new DAOField("TipoCategoriaGasto_id", java.sql.Types.INTEGER, true));
        addFieldDefinition(new DAOField("Descripcion", java.sql.Types.VARCHAR, 200));

    }
}
