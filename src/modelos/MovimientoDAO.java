package modelos;

import com.alexco.db.dao.DAOBase;
import com.alexco.db.dao.DAOField;
import java.sql.Date;

public class MovimientoDAO extends DAOBase {

    private int cuenta_id;
    private int movimiento;

    public int getMovimiento() {
        return movimiento;
    }

    public void setMovimiento(int movimiento) {
        this.movimiento = movimiento;
    }

    private String descripcion;
    private Date fecha;
    private double valor;
    private char tipo;
    private int tarjeta_id;
    private int credito_id;
    private int prestamo_id;

    public int getCuenta_id() {
        return cuenta_id;
    }

    public void setCuenta_id(int cuenta_id) {
        this.cuenta_id = cuenta_id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public char getTipo() {
        return tipo;
    }

    public void setTipo(char tipo) {
        this.tipo = tipo;
    }

    public int getTarjeta_id() {
        return tarjeta_id;
    }

    public void setTarjeta_id(int tarjeta_id) {
        this.tarjeta_id = tarjeta_id;
    }

    public int getCredito_id() {
        return credito_id;
    }

    public void setCredito_id(int credito_id) {
        this.credito_id = credito_id;
    }

    public int getPrestamo_id() {
        return prestamo_id;
    }

    public void setPrestamo_id(int prestamo_id) {
        this.prestamo_id = prestamo_id;
    }

    public int getConceptoIngreso_id() {
        return conceptoIngreso_id;
    }

    public void setConceptoIngreso_id(int conceptoIngreso_id) {
        this.conceptoIngreso_id = conceptoIngreso_id;
    }

    public int getConceptoGasto_id() {
        return conceptoGasto_id;
    }

    public void setConceptoGasto_id(int conceptoGasto_id) {
        this.conceptoGasto_id = conceptoGasto_id;
    }

    public int getTransferencia_id() {
        return transferencia_id;
    }

    public void setTransferencia_id(int transferencia_id) {
        this.transferencia_id = transferencia_id;
    }
    private int conceptoIngreso_id;
    private int conceptoGasto_id;
    private int transferencia_id;

    public MovimientoDAO() {
        super("Movimientos");
        addFieldDefinition(new DAOField("Movimiento_id", java.sql.Types.INTEGER, true));
        addFieldDefinition(new DAOField("Cuenta_id", java.sql.Types.INTEGER, false));
        addFieldDefinition(new DAOField("Descripcion", java.sql.Types.VARCHAR, 200));
        addFieldDefinition(new DAOField("Fecha", java.sql.Types.DATE));
        addFieldDefinition(new DAOField("Valor", java.sql.Types.INTEGER, false));
        addFieldDefinition(new DAOField("Tipo", java.sql.Types.CHAR));
        addFieldDefinition(new DAOField("Tarjeta_id", java.sql.Types.INTEGER, false));
        addFieldDefinition(new DAOField("Credito_id", java.sql.Types.INTEGER, false));
        addFieldDefinition(new DAOField("Prestamo_id", java.sql.Types.INTEGER, false));
        addFieldDefinition(new DAOField("ConceptoIngreso_id", java.sql.Types.INTEGER, false));
        addFieldDefinition(new DAOField("ConceptoGasto_id", java.sql.Types.INTEGER, false));
        addFieldDefinition(new DAOField("Transferencia_id", java.sql.Types.INTEGER, false));

    }

}
