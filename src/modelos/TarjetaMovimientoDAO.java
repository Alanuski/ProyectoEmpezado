package modelos;

import com.alexco.db.dao.DAOBase;
import com.alexco.db.dao.DAOField;
import java.sql.Date;

public class TarjetaMovimientoDAO extends DAOBase {

    private int tarjeta_id;
    private String descripcion;
    private Date fecha;
    private double valor;
    private boolean esProcesado;
    private int tarjetaMovimiento_id;

    public int getTarjetaMovimiento_id() {
        return tarjetaMovimiento_id;
    }

    public void setTarjetaMovimiento_id(int tarjetaMovimiento_id) {
        this.tarjetaMovimiento_id = tarjetaMovimiento_id;
    }

    public int getTarjeta_id() {
        return tarjeta_id;
    }

    public void setTarjeta_id(int tarjeta_id) {
        this.tarjeta_id = tarjeta_id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public boolean isEsProcesado() {
        return esProcesado;
    }

    public void setEsProcesado(boolean esProcesado) {
        this.esProcesado = esProcesado;
    }

    public TarjetaMovimientoDAO() {
        super("TarjetasMovimientos");
        addFieldDefinition(new DAOField("TarjetaMovimiento_id", java.sql.Types.INTEGER, true));
        addFieldDefinition(new DAOField("Descripcion", java.sql.Types.VARCHAR, 200));
        addFieldDefinition(new DAOField("Tarjeta_id", java.sql.Types.INTEGER, false));
        addFieldDefinition(new DAOField("Descripcion", java.sql.Types.VARCHAR, 200));
        addFieldDefinition(new DAOField("Fecha", java.sql.Types.DATE));
        addFieldDefinition(new DAOField("Valor", java.sql.Types.DOUBLE));
        addFieldDefinition(new DAOField("TipoTAE", java.sql.Types.BOOLEAN));
    }

}
